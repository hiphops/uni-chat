/*- coding = utf-8 -*-
@Time : 2023/2/1 10:19
@Author : 沉默小管
@File : index.ts
@web  : golangblog.blog.csdn.net
@Software: WebStorm
*/

export {UserAddDto} from "./userAdd.dto"
export {UserListDto} from "./userList.dto"
export {UserUpdateDto} from "./userUpdate.dto"
export {UserDelDto} from "./userDel.dto"
