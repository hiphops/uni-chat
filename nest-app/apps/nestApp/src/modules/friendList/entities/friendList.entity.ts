import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import { BaseEntity } from "@/apps/common/commonModules/entities/base.entity";

@Entity('g_friendList')
export class FriendListEntity extends BaseEntity{
    @PrimaryGeneratedColumn({
        comment:"通讯列表",
    })//主键
    id:number;

    @Column({
        type:"int",
        nullable:true,
        comment:"自己的id",
    })
    userId: number;

    @Column({
        type:"int",
        length:200,
        comment:"好友id",
    })
    friendId: number;

    @Column({
        type:"varchar",
        length:200,
        nullable:true,
        comment:"好友备注名称(默认为用户名称)",
    })
    friendNickName: string;

    @Column({
        type:"text",
        nullable:true,
        comment:"好友图片",
    })
    friendHeadImg: string;

}
