/*- coding = utf-8 -*-
@Time : 2023/2/1 10:19
@Author : 沉默小管
@File : index.ts
@web  : golangblog.blog.csdn.net
@Software: WebStorm
*/

export {FriendListAddDto} from "./friendListAdd.dto"
export {FriendListDto} from "./friendList.dto"
export {FriendListUpdateDto} from "./friendListUpdate.dto"
export {FriendListDelDto} from "./friendListDel.dto"
