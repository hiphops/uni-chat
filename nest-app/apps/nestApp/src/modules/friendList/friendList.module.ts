import { Module } from '@nestjs/common';
import { FriendListService } from './friendList.service';
import { FriendListController } from './friendList.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {FriendListEntity} from "./entities/friendList.entity";

@Module({
  imports:[TypeOrmModule.forFeature([FriendListEntity])],
  controllers: [FriendListController],
  providers: [FriendListService]
})
export class FriendListModule {}
