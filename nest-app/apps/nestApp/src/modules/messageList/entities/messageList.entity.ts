import { BaseEntity } from "@/apps/common/commonModules/entities/base.entity";
import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import { statusEnum } from "@/common/enum";

@Entity('g_msgList')
export class MessageListEntity extends BaseEntity{
    @PrimaryGeneratedColumn({
        comment:"消息列表",
    })//主键
    id:number;

    @Column({
        type:"int",
        nullable:true,
        comment:"接收者id",
    })
    toUserId: number;

    @Column({
        type:"int",
        nullable:true,
        comment:"发送者id",
    })
    formUserId: number;

    @Column({
        type:"varchar",
        length:200,
        nullable:true,
        comment:"消息内容",
    })
    message: string;

    @Column({
        type:"tinyint",
        nullable:true,
        comment:"接收状态(1接收，2未接受)",
    })
    status: number;

    @Column({
        type:"varchar",
        length:255,
        nullable:true,
        comment:"发送时间(通过该时间判断最新一条消息)",
    })
    sendTime: string;

}
