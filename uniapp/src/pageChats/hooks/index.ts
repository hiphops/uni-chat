/*- coding = utf-8 -*-
@Time : 2023/6/29 8:47
@Author : 管茂良
@File : index.ts
@web  : www.php-china.com
@Software: WebStorm
*/

import {ref} from "vue";

export function handleUseDefer(maxCount: number){
    console.log(maxCount,"maxCountmaxCountmaxCount");
    let frameCount = ref<number>(0)
    //更新帧数
    function handleUpdateFrameCount(){
        requestAnimationFrame(()=>{
            frameCount.value++;
            handleUpdateFrameCount();
        })
    }
    handleUpdateFrameCount();
    console.log(frameCount.value,"frameCountframeCountframeCount");
    return function defer(n:number){
        console.log(n,"nnnnnnnnnnnnnnnnnn");
        return frameCount.value >= n
    }
}
