import { createSSRApp } from "vue";
import App from "./App.vue";
import plugins from "./plugins"

import 'virtual:svg-icons-register'

export function createApp() {
  const app = createSSRApp(App);
    plugins(app)

  return {
    app,
  };
}
