/*- coding = utf-8 -*-
@Time : 2022/9/21 14:27
@Author : 管茂良
@File : app.tsx
@web  : www.php-china.com
@Software: WebStorm
*/
import {defineStore} from "pinia"
import storeName from "@/store/storeName"


const useAppStore = defineStore(storeName.app,{
    state:()=>({

    }),
    getters:{

    },
    actions:{

    }
})

export default useAppStore