//*- coding = utf-8 -*-
//@Time : 2022-10-19 0:20
//@Author : 管茂良
//@File : index.js
//@web  : www.php-china.com
//@Software: WebStorm
import {request} from "@/network/request"

//医生列表
export function requestDoctorList(){
    return request({
        url:"/video/tiktok/selectDoctorListPublic",
        methods:"GET",
    })
}
//预约
export function requestAppointment(data:any){
    return request({
        url:"/video/tiktok/saveFormPublic",
        methods:"POST",
        data,
    })
}
//医生详情
export function requestDoctorDetail(id:any){
    return request({
        url:"/video/tiktok/getDetailByIdPublic?id="+id,
        methods:"GET",
    })
}