/*- coding = utf-8 -*-
@Time : 2023/7/4 14:27
@Author : 管茂良
@File : index.ts
@web  : www.php-china.com
@Software: WebStorm
*/

import { h, ref, render} from "vue";
import uNotify from "@/uni_modules/uview-plus/components/u-notify/u-notify.vue"
import {typeType} from "@/plugins/model/notify/types/index";

interface propsInterface{
    message:string,
    type?:typeType,
    duration?:number,
    safeAreaInsetTop?:boolean,
}
const parentNode = document.createElement('div')
parentNode.setAttribute("id","notify-style")
let optionsInit:propsInterface = {
    message:"",
    type:"primary",
    duration:2000,
    safeAreaInsetTop:false,
}
let options = ref<propsInterface>(optionsInit)

const handleNotifyRender = () => {
    // 创建 虚拟dom
    const boxVNode = h(uNotify, {...options.value})
    // 将虚拟dom渲染到 container dom 上
    render(boxVNode, parentNode)
    // 最后将 container 追加到 body 上
    document.body.appendChild(parentNode)
    return boxVNode;
}

let modalInstance: any

const handleNotify = () => {
    const show = (props:propsInterface)=>{
        if (modalInstance) {
            const notifyVue = modalInstance.component
            // 调用上述组件中定义的open方法显示弹框
            // 注意不能使用notifyVue.ctx来调用组件函数（build打包后ctx会获取不到）
            notifyVue.proxy.show(props);
        } else {
            options.value = Object.assign(options.value,props)
            modalInstance = handleNotifyRender()
            show(options.value)
        }
    }
    const hide = ()=>{
        if (modalInstance) {
            const notifyVue = modalInstance.component
            // 调用上述组件中定义的open方法显示弹框
            // 注意不能使用notifyVue.ctx来调用组件函数（build打包后ctx会获取不到）
            notifyVue.proxy.close();
        } else {
            modalInstance = handleNotifyRender()
            hide()
        }
    }
    return {show,hide}
}

export default handleNotify
