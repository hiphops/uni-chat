/*- coding = utf-8 -*-
@Time : 2023/7/4 14:27
@Author : 管茂良
@File : index.ts
@web  : www.php-china.com
@Software: WebStorm
*/

import Loading from "@/components/loading/index"
import {createApp, ref} from "vue";
import {modeType} from "@/components/loading/types";
interface propsInterface{
    isShowLoading?:boolean,
    loadingMode?:modeType,
    loadingSize?:number,
    loadingTitle?:string,
}
const parentNode = document.createElement('div')
parentNode.setAttribute("id","loading-style")
let options = ref<propsInterface>({
    isShowLoading:false,
    loadingMode:"circle",
    loadingSize:60,
    loadingTitle:"加载中...",
})
const handleLoad = ()=> {
    const loadingInstance =(options?:any)=>{
        return createApp(Loading,{
            ...options
        })
    }
    const show = (obj?:propsInterface)=>{
        options.value = {...obj}
        options.value.isShowLoading = true;
        document.body.appendChild(parentNode)
        loadingInstance(options.value).mount(parentNode)
    }
    const hide = () => {
        options.value.isShowLoading = false;
        let loadingStyle = document.querySelector("#loading-style")
        document.body.removeChild(loadingStyle)
    }
    return {show,hide}
}
export default handleLoad
