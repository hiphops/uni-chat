/*- coding = utf-8 -*-
@Time : 2023/7/4 16:56
@Author : 管茂良
@File : index.ts
@web  : www.php-china.com
@Software: WebStorm
*/
export type modeType = "circle" | "spinner" | "semicircle"
