import { Plugin } from 'vite'
import autoImport from './autoImport'
import setupUni from './uni'
import svgIcons from './svgIcons'

const plugins: Plugin[] = []

export default function setupPlugins(isBuild: boolean) {
    autoImport(plugins, isBuild )
    setupUni(plugins, isBuild )
    svgIcons(plugins, isBuild )

    return plugins
}
