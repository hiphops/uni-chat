import { defineConfig } from "vite";
import setupPlugins from "./vite/plugins";
import { join } from 'path'
import {config} from "./.env"
const postcssPresetEnv = require("postcss-preset-env")

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
	const isBuild = command == 'build'
	return {
		 plugins: [...setupPlugins(isBuild)],
		//设置别名
		resolve: {
			alias: {
                "@": join(__dirname, 'src'),
			},
			extensions: [".vue",'.js', '.json', '.ts', '.tsx','.mjs']//使用别名省略的后缀名
		},
		server: {//代理
		    proxy: {
		        "/dev-api/": {
					target: config.BASE_API,
		            changeOrigin: true,
		            rewrite: (path) => path.replace(/^\/dev-api/, ""),
		        },
		    },
		},
        css: {//设置全局引用
            preprocessorOptions: {
                less: {
                    javascriptEnabled: true,
                    additionalData: `
                    @import "${join(__dirname, 'src/static/css/base.less')}";
                    @import "${join(__dirname, 'src/static/css/system-variables.less')}";
                    `
                },
            },
			postcss:{
				plugins:[postcssPresetEnv()]
			}
        },
        define:{
		     "process.env":{}
        }
	}

});
